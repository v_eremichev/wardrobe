class EditGoodPage < SitePrism::Page
  set_url "/goods/{good_id}/edit"
  set_url_matcher %r{/goods/\d/edit}

  element :name, "#good_name"
  element :description, "#good_description"
  element :season, "#good_season"
  element :image, "#good_image"
  element :save_button, "input[value='Update Good']"

  def fill_form_with (args)
    name.set args[:name]
    description.set args[:description]
    season.set args[:season]
    season.select args[:season]
    attach_file("good_image", args[:image_path])
  end
end
