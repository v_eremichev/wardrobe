class GoodPage < SitePrism::Page
  set_url "/goods{/good_id}"

  element :edit_link, "a#edit"
  element :back_link, "a#back"
end
