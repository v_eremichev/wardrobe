class NewGoodPage < SitePrism::Page
  set_url "/goods/new"
  set_url_matcher %r{/goods/new}

  element :name, "#good_name"
  element :description, "#good_description"
  element :season, "#good_season"
  element :image, "#good_image"
  element :create_button, "input[value='Create Good']"

  def fill_form_with (args)
    name.set args[:name]
    description.set args[:description]
    season.select args[:season]
    attach_file("good_image", args[:image_path])
  end
end
