
FactoryGirl.define do

  factory :good do
    sequence(:name) { |n| "good#{n}" }
    description "some description"
    season 1
    image { File.new("#{Rails.root}/app/assets/images/tshirt.jpg")}
  end

end
