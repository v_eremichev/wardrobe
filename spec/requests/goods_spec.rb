require 'rails_helper'

RSpec.feature "Goods",:type => :feature, js: true do
  let!(:good){ create :good, season: Good.seasons(true)[:winter] }

  before do
    visit goods_path
  end

  context "show by ctegories" do
    let!(:summer_good){ create :good, season: Good.seasons(true)[:summer] }
    before do
      visit goods_path
    end

    scenario "show All of goods" do
      expect(page).to have_content(good.name)
      expect(page).to have_content(good.description)

      expect(page).to have_content(summer_good.name)
      expect(page).to have_content(summer_good.description)

      within("#good-#{good.id}") do
        expect(page).to have_link( href: good_path(good))
        expect(page).to have_link("Edit", href: edit_good_path(good))
        expect(page).to have_link("Destroy", href: good_path(good))
      end

      within("#good-#{summer_good.id}") do
        expect(page).to have_link( href: good_path(summer_good))
        expect(page).to have_link("Edit", href: edit_good_path(summer_good))
        expect(page).to have_link("Destroy", href: good_path(summer_good))
      end
    end

    scenario "show winter goods" do
      select "Winter", from: "season_id"
      expect(page).to have_content(good.name)
      expect(page).to_not have_content(summer_good.name)

      select "All", from: "season_id"
      expect(page).to have_content(good.name)
      expect(page).to have_content(summer_good.name)
    end

    scenario "show summer goods" do
      select "Summer", from: "season_id"
      expect(page).to_not have_content(good.name)
      expect(page).to have_content(summer_good.name)
    end

    scenario "show spring goods" do
      select "Spring", from: "season_id"
      expect(page).to_not have_content(good.name)
      expect(page).to_not have_content(summer_good.name)
    end
  end

  scenario "image should lead to good" do
    page.find(".img-thumbnail").click
    expect(page).to have_content(good.name)
  end

  scenario "edit link should lead to edit page" do
    edit_page = EditGoodPage.new
    click_link "Edit"
    expect(edit_page.name.value).to eq(good.name)
    expect(edit_page).to have_content("Editing Good")
  end

  scenario "remove good" do
    accept_confirm do
      click_link 'Destroy'
    end
    expect(page).to_not have_content(good.name)
  end

  context "show good page" do

    before do
      visit good_path(good)
    end

    scenario "should contain good info" do
      expect(page).to have_content(good.name)
      expect(page).to have_content(good.description)
      expect(page).to have_content(Good.seasons(true).key(good.season))
      expect(page).to have_link "Edit"
      expect(page).to have_link "Back"
    end

    scenario "should have modal" do
      page.find(".thumbnail").click
      expect(page).to have_css("div.in")
      within(".modal-header") do
        page.find("span").click
      end
      expect(page).to_not have_css("div.in")
    end
  end


  context "edit good" do
    let!(:edit_page){ EditGoodPage.new }
    let!(:good_page){ GoodPage.new }

    scenario "with valid data" do
      edit_page.load(good_id: good.id)
      edit_page.name.set "new good name"
      edit_page.save_button.click
      expect(good_page).to have_content("Good was successfully updated.")
      expect(good_page).to have_content("new good name")
    end
  end

  context "create new good" do
    let!(:create_page){ NewGoodPage.new }
    let!(:good_page){ GoodPage.new }

    before do
      create_page.load
    end

    scenario "with valid data" do

      g = {name:"sweather", description:"wool, warm", season: "Summer", image_path:"#{Rails.root}/app/assets/images/tshirt.jpg"}
      create_page.fill_form_with g
      create_page.create_button.click

      expect(good_page).to be_all_there
      expect(good_page).to have_content "Good was successfully created."
      expect(good_page).to have_content g[:name]
      expect(good_page).to have_link("Edit", href: edit_good_path(Good.last.id))
      expect(good_page).to have_link("Back", href: goods_path)
    end

    scenario "neme should be required" do
      g = {name:"", description:"wool, warm", season: "Summer", image_path:"#{Rails.root}/app/assets/images/tshirt.jpg"}
      create_page.fill_form_with g
      create_page.create_button.click
      expect(create_page).to have_content "Name can't be blank"
    end
  end

end
