require 'rails_helper'

RSpec.describe "MainPages" do
  it "should open page" do
    visit root_path
    expect(page).to have_link("Show goods", href: goods_path)
  end
end
