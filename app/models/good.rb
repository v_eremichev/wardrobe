class Good < ApplicationRecord
  has_attached_file :image, styles: {medium: "300x300>", thumb: "50x50>"},
                    url: "/assets/products/:id/:style/:basename.:extension",
                    path: ":rails_root/public/assets/products/:id/:style/:basename.:extension",
                    default_url: "http://1.bp.blogspot.com/-LJv5_H0e5nM/UzAqr7as75I/AAAAAAAAAsM/f9_6eUuUuo8/s1600/clothes+1.png"

  validates_attachment_size :image, :less_than => 5.megabytes
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/
  validates :name, presence: true

  scope :winter, -> { where(season: 0) }
  scope :spring, -> { where(season: 1) }
  scope :summer, -> { where(season: 2) }
  scope :autumn, -> { where(season: 3) }



  def self.seasons (h = false)
    if h
      { winter: 0, spring: 1, summer: 2, autumn: 3 }
    else
      [['Winter', 0], ['Spring', 1], ['Summer', 2], ['Autumn', 3]]
    end
  end

end
