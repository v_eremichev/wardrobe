json.extract! good, :id, :name, :description, :season, :created_at, :updated_at
json.url good_url(good, format: :json)
