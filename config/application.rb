require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Wardrobe
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
  end
end
Rails.application.configure do
  config.generators do |g|
    g.test_framework :rspec
    g.integration_tool :rspec
  end
  config.assets.precompile += %w(*.png *.jpg *.jpeg *.gif)
end

