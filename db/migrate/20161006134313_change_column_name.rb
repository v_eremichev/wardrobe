class ChangeColumnName < ActiveRecord::Migration[5.0]
  def change
    rename_column :goods, :type, :season
  end
end
